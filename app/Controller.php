<?php
class Controller {

  protected $db; // database object
  protected $db_config; // database object
  protected $db_status; // database object

  const STATUS_OK       = 0;
  const STATUS_DEGRADED = 1;
  const STATUS_FATAL    = 2;
  const STATUS_UNKNOWN  = 3;

  function __construct($f3) {
    $this->db = new \DB\Jig ('../data/', \DB\Jig::FORMAT_JSON);
    $this->db_config = new \DB\Jig\Mapper($this->db, 'config.json');
    $this->db_status = new \DB\Jig\Mapper($this->db, 'status.json');

    // load config
    $this->db_config->load(array('@_id = ?', 'main'));
    if (!$this->db_config->dry()) {
      $f3->set('SITE.NAME', isset($this->db_config->site_name) ? $this->db_config->site_name : null);
      $f3->set('CACHE', isset($this->db_config->cache_enable) ? $this->db_config->cache_enable : FALSE);
    }
  }

  /****************************************************************************
   * This takes care of the final page rendering steps. Rather than repeating
   * these same lines in every controller function, we have this short helper
   * to reduce page rendering steps to a single function call.
   ***************************************************************************/
  public function RenderPage($content, $title, $header = null) {
    $f3 = Base::instance();
    $header = $header ?: $title; // default header to title if header not set
    $f3->set('PAGE.TITLE', $title);
    $f3->set('PAGE.HEADER', $header);
    $f3->set('PAGE.CONTENT', $content);
    echo \Template::instance()->render('layout.htm');
  }
  
  /****************************************************************************
   * custom error handler for fatfreeframework
   ***************************************************************************/
  function error_handler($f3) {
    // recursively clear existing output buffers:
    while (ob_get_level())
      ob_end_clean();
    
    printf("%s: %s (HTTP %s)\n",
      $f3->get('ERROR.status'),
      $f3->get('ERROR.text'),
      $f3->get('ERROR.code'),
    );
  }

  /****************************************************************************
   * Given an epoch, calculates a relative "X (sec|min|hour|etc) ago" string.
   * Handles both future and past timestamps
   ***************************************************************************/
  public function ts2age($ts) {
    if(!ctype_digit($ts))
      $ts = strtotime($ts);

    $diff = time() - $ts;
    if($diff == 0) {
      return 'now';
    } elseif($diff > 0) {
      // in the past
      $day_diff = floor($diff / 86400);
      if($day_diff == 0) {
        if($diff < 60) return 'just now';
        if($diff < 120) return '1 minute ago';
        if($diff < 3600) return floor($diff / 60) . ' minutes ago';
        if($diff < 7200) return '1 hour ago';
        if($diff < 86400) return floor($diff / 3600) . ' hours ago';
      }
      if($day_diff == 1) { return 'Yesterday'; }
      if($day_diff < 7) { return $day_diff . ' days ago'; }
      if($day_diff < 31) { return ceil($day_diff / 7) . ' weeks ago'; }
      if($day_diff < 60) { return 'last month'; }
      return date('F Y', $ts);
    } else {
      // in the future
      $diff = abs($diff);
      $day_diff = floor($diff / 86400);
      if($day_diff == 0) {
        if($diff < 120) { return 'in a minute'; }
      if($diff < 3600) { return 'in ' . floor($diff / 60) . ' minutes'; }
      if($diff < 7200) { return 'in an hour'; }
      if($diff < 86400) { return 'in ' . floor($diff / 3600) . ' hours'; }
      }
      if($day_diff == 1) { return 'Tomorrow'; }
      if($day_diff < 4) { return date('l', $ts); }
      if($day_diff < 7 + (7 - date('w'))) { return 'next week'; }
      if(ceil($day_diff / 7) < 4) { return 'in ' . ceil($day_diff / 7) . ' weeks'; }
      if(date('n', $ts) == date('n') + 1) { return 'next month'; }
      return date('F Y', $ts);
    }
  }

  /****************************************************************************
   * Validates if a value is a valid integer. Does not consider the type of
   * the passed variable, only the value (eg, string "123" is an integer).
   ***************************************************************************/
  function isInteger($i){
    return filter_var($i, FILTER_VALIDATE_INT);
  }
}

<?php
class Manage extends Controller {
  
  /****************************************************************************
   * Settings page for administrator to configure the project.
   ***************************************************************************/
  function index($f3) {
    $f3->error(418); // TODO
  }

  /****************************************************************************
   * handles a POST request to add a new node
   ***************************************************************************/
  function add_node($f3) {
    $name = $f3->get('POST.name');
    $order= $f3->get('POST.order');

    // validate user input
    if (strlen($name) === 0)
      $f3->error(400, 'error.user.no_name');
    if ($this->isInteger($order) === false)
      $f3->error(400, 'error.user.invalid_order');

    // create the jig database object
    $db_status = new \DB\Jig\Mapper($this->db, 'status.json');

    $db_status->node = bin2hex(openssl_random_pseudo_bytes(4));
    $db_status->name = $name;
    $db_status->order = $order;
    $db_status->status = self::STATUS_UNKNOWN;
    $db_status->update_ts = time();
    $db_status->update_ip = $f3->get('IP');
    $db_status->insert();

    echo $db_status->node;
  }

}

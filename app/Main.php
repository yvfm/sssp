<?php
class Main extends Controller {
  
  /****************************************************************************
   * Renders the public status overview page
   ***************************************************************************/
  function index($f3) {
    #$db_status = new \DB\Jig\Mapper($this->db, 'status.json');

    // load records from the database
    $this->db_status->load(
      null,
      array('order'=>'order')
    );
    if ($this->db_status->dry())
      $f3->error(404, "error.db.empty");

    $nodes = array();
    $highest_failure_event = 0;
    $most_recent_update_ts = 0;
    while (!$this->db_status->dry()) {
      $dbid = $this->db_status->_id;

      // validate this row from the database
      if (isset($this->db_status->name) === false)
        $f3->error('500', "error.data.$dbid.no_name");
      if (isset($this->db_status->status) === false)
        $f3->error('500', "error.data.$dbid.no_status");
      if (isset($this->db_status->update_ts) === false)
        $f3->error('500', "error.data.$dbid.no_timestamp");

      // append this system to the array for the template
      $nodes[] = array(
        'name'    => $this->db_status->name,
        'msg'     => (isset($this->db_status->msg) ? $this->db_status->msg : null),
        'status'  => $this->db_status->status,
        'ts'      => $this->db_status->update_ts,
        'age'     => $this->ts2age($this->db_status->update_ts),
      );

      // calculate summary data
      if ($this->db_status->status > $highest_failure_event)
        $highest_failure_event = $this->db_status->status;
      if ($this->db_status->update_ts > $most_recent_update_ts)
        $most_recent_update_ts = $this->db_status->update_ts;

      $this->db_status->next();
    }
    $f3->set('nodes', $nodes);
    $f3->set('highest_failure_event', $highest_failure_event);
    $f3->set('most_recent_update_ts', $most_recent_update_ts);
    $f3->set('most_recent_update_str', $this->ts2age($most_recent_update_ts));
    $this->RenderPage('status.htm', 'Status');
  }
  
  
  /****************************************************************************
   * Handles POST requests to update the database for a node. Requests can be
   * made programatically (eg, curl) or from the manual update HTML form.
   ***************************************************************************/
  function update($f3) {
    $nid    = $f3->get('POST.nid');
    $status = $f3->get('POST.status');
    $msg    = $f3->get('POST.msg');
    $key    = $f3->get('POST.key');
    
    // validate key before doing anything else
    if (!$key)
      $f3->error(401, 'error.user.no_key');
    $db_keys = new \DB\Jig\Mapper($this->db, 'keys.json');
    $db_keys->load(array('@key = ?', $key));
    if ($db_keys->dry())
      $f3->error(401, 'error.user.bad_key');

    // validate user input
    if (preg_match('/^[0-9a-f\.]+$/i', $nid) == 0)
      $f3->error(400, 'error.user.invalid_nid');
    if (preg_match('/^[0-3]$/', $status) == 0)
      $f3->error(400, 'error.user.invalid_status');

    // load the database record for update
    $this->db_status->load(array('@node = ?', $nid));
    if ($this->db_status->dry())
      $f3->error(404, "error.node.not_found");

    // make the changes and save back to the database
    $this->db_status->status = $status;
    $this->db_status->msg = $msg;
    $this->db_status->update_ts = time();
    $this->db_status->update_ip = $f3->get('IP');
    $this->db_status->update();
    $this->db_status->reset();

    if ($f3->get('POST.manual_update') == 1)
      $f3->reroute('@home');
    else
      $f3->status(204); // http status "no content"
  }

  
  /****************************************************************************
   * Allows administrator to post manual updates rather than programatically
   * posting them using curl etc. This basically just emulates the POST request
   * by giving the user a form that is then submitted to the normal POST route.
   ***************************************************************************/
  function manual_update($f3) {
    // load data for the user view
    $f3->set('nodes', $this->db_status->find(
      null,
      array('order'=>'order')
    ));
      
    $this->RenderPage('update.htm', 'Manual Update');
  }
  
}

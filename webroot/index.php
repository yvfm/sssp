<?php

// initialize dependencies using the composer autoloader
require __DIR__ . '/../vendor/autoload.php';
$f3 = \Base::instance();

$f3->set('DEBUG', 0);
$f3->set('AUTOLOAD', '../app/');
$f3->set('UI', '../views/');

// setup fat free framework routes
$f3->route('GET  @home:        /',        'Main->index');         // public status page
$f3->route('POST @post_update: /',        'Main->update');        // status update post
$f3->route('GET  /update',  'Main->manual_update'); // manual status update
$f3->route('GET  /manage',  'Manage->index');       // settings
$f3->route('POST /manage',  'Manage->index');       // settings

// custom error handler
$f3->set('ONERROR', 'Controller->error_handler');

// kick off the f3 engine to do the heavy lifting
$f3->run();

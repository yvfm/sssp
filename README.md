# SSSP - Stupid Simple Status Page

A basic HTML page to display status of a 'system' - that is a network, website,
infrastructure etc. Whatever you deem to be a 'system'.

Basic PHP code with a JSON database for persistence, SSSP has very few
dependencies.

The system tracks and displays status and basic update information for
'nodes' - each node is a display item on the status page. A node is just a unit
of work - it could refer to a whole server, and individual process, a
container, or a scheduled task (eg, backup tasks).

Each node can be in one of 4 states:

  * Operational (0)
  * Degraded (1)
  * Not Operational (2)
  * Unknown (3)

Any nodes in a state other than 0 display a text message to give the reader
some context as to why it is Degraded/Not Operational/Unknown.

## Dynamic Updates

Updates can be dynamically/programatically updated using HTTP POST requests
to the hosted page. Example curl commands below.

Change the status of a system; required argument are:

  * the node id (`nid`)
  * `status`
  * descriptive message (`msg`) - only if status is not equal to 0.

Example:

    curl -X POST -d 'nid=1234abcd&status=1&msg=Power supply has failed.' https://status.example.com/

## Installation

  1. Clone repo to an environment with PHP and a HTTP server (eg, apache, nginx etc)
  2. Run `composer install`
  3. Create directories writable by your HTTP/PHP processes: `install -Dm0755 -o nginx data/ webroot/tmp/`
  4. Point your web server/virtual host to the 'webroot' directory.
